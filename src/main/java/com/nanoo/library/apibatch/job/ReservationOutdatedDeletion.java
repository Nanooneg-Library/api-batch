package com.nanoo.library.apibatch.job;

import com.nanoo.library.apibatch.web.proxy.FeignProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author nanoo
 * @created 06/04/2020 - 17:42
 */
@Service
public class ReservationOutdatedDeletion {

    private Logger logger = LoggerFactory.getLogger(ReservationOutdatedDeletion.class);

    private FeignProxy proxy;

    @Autowired
    public ReservationOutdatedDeletion(FeignProxy proxy) {
        this.proxy = proxy;
    }

    /**
     *  This method ask ms-loan to update loan status
     *
     * @param accessToken security token
     */
    public Map<String,String> removeOldReservationAndGetNext(String accessToken){

        logger.info("*** removeOldReservationAndGetNext task begin ***");

        return proxy.doReservationOutdatedDeletionAndGetNext(accessToken);

    }

}
