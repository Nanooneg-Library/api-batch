package com.nanoo.library.apibatch.web.controller;

import com.nanoo.library.apibatch.task.NotificationScheduledTask;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author nanoo
 * @created 06/04/2020 - 17:51
 */
@RestController
@RequestMapping("/launch")
public class ExternalLauncherController {

    private final NotificationScheduledTask notificationTask;

    @Autowired
    public ExternalLauncherController(NotificationScheduledTask notificationTask) {
        this.notificationTask = notificationTask;
    }

    @GetMapping("/notification")
    public void launchNotificationForNextClient(){
        notificationTask.runScheduledTask();
    }
}
